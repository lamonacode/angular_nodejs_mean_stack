const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const postsRoutes = require("./routes/posts");
const userRoutes = require("./routes/user");

const app = express();

mongoose
  .connect(
    "mongodb+srv://jlamonaco:ubEZzymgas5Hw1TG@cluster0-aa9z2.mongodb.net/node-angular?retryWrites=true", { useNewUrlParser: true }
  )
  .then(() => {
    console.log("Connected to database!");
  })
  .catch(() => {
    console.log("Connection failed!");
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/images", express.static(path.join("backend/images"))); // allows access to this path

// this allows us to avoid CORS errors
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});


app.use("/api/posts", postsRoutes);
app.use("/api/user", userRoutes);

//  these functions are moved into posts.js
// app.put("/api/posts/:id", (req, res, next) => {...
// app.get("/api/posts", (req, res, next) => {...
// app.get('/api/posts/:id', (req, res, next) => {...
// app.delete("/api/posts/:id", (req, res, next) => {...

module.exports = app;
